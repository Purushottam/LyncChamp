//
//  DBCreater.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/28/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AnswerOption.h"
#import "SlotCategroy.h"
#import "SlotItem.h"
#import "TruthTableEntry.h"
#import "SeedTableEntry.h"

@class DBCreater;

@protocol DBCreaterDelegate <NSObject>

- (void)creater:(DBCreater *)creater sucessfullyDownloadedSeedData:(NSArray *)seedData;
- (void)createrFailedToDownloadSeedData:(DBCreater *)creator;

- (void)createrSucessfullyDownloadedAnswer:(DBCreater *)creater;
- (void)createrFailedDownloadingAnswer:(DBCreater *)creater;

- (void)createrSucessfullyDownloadedCategory:(DBCreater *)creater;
- (void)createrFailedDownloadingCategory:(DBCreater *)creater;

- (void)createrSucessfullyDownloadedSlotItems:(DBCreater *)creater;
- (void)createrFailedDownloadingSlotItems:(DBCreater *)creater;

- (void)createrSucessfullyDownloadedTruthTable:(DBCreater *)creater;
- (void)createrFailedDownloadingTruthTable:(DBCreater *)creater;

@end

@interface DBCreater : NSObject

@property (weak, nonatomic) id<DBCreaterDelegate> delegate;

- (void)copyDBToDoc;
- (void)updateAnswerTable;
- (void)updateCategoriesTable;
- (void)updateSlotItems;
- (void)updateTruthTable;
- (void)downloadSlotImages;

- (void)getSeedDataAndShouldUpdate:(BOOL)update;
- (void)updateLocalSeedTable;

@end
