//
//  ViewController.m
//  SlotNew
//
//  Created by Vmoksha on 31/10/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "ViewController.h"
#import "ZCSlotMachine.h"
#import "CustomAlertView.h"
#import "DBManager.h"
#import "SlotItem.h"
#import "TruthTableEntry.h"
#import "SyncHelper.h"

#define MAX_NO_ATTEMPTS 10
#define MAX_TIME_ALLOTTED 300 //compensation for first

@interface ViewController () <ZCSlotMachineDataSource, ZCSlotMachineDelegate, UIAlertViewDelegate, CustomAlertViewDelegate, MFMailComposeViewControllerDelegate, SyncHelperDelegate, UIAlertViewDelegate>
{
    NSArray *slot1Items;
    NSArray *slot2Items;
    NSArray *slot3Items;
    
    int spinCount;
    int numOfAttempt;
    BOOL isAttempted;
    BOOL haveShownScoreAlert;
    NSInteger score;
    
    CustomAlertView *customAlertView;
    TruthTableEntry *currentTruthTableEntry;
    NSString *currentlySelectedAnswerCode;
    
    NSMutableArray *repeatSlotCombinationArr;
    UIAlertView *updationAlert;
    
    NSInteger _timeLeft;
    NSTimer *timer;
}

@property (strong, nonatomic) IBOutlet UIView *slotContainer;
@property (strong, nonatomic) IBOutlet ZCSlotMachine *slotOutlet1;
@property (strong, nonatomic) IBOutlet ZCSlotMachine *slotOutlet2;
@property (strong, nonatomic) IBOutlet ZCSlotMachine *slotOutlet3;
@property (strong, nonatomic) IBOutlet UIButton *spinOutlet;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundImgViweOutlet;
@property (strong, nonatomic) IBOutlet UIButton *refreshBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *excelentBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *riskyBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *justDontBtnOutlet;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *noOfAttemptsLabel;
@property (strong, nonatomic) IBOutlet UIImageView *UCBLogoImg;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *refreshLeftConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *helpRightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *exceToRiskConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *riskT0JustDntConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *execlLeftConst;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *howCenterAlineConstraint;

@property (strong, nonatomic) IBOutlet UILabel *whereLbl;
@property (strong, nonatomic) IBOutlet UILabel *whatLbl;
@property (strong, nonatomic) IBOutlet UILabel *howLbl;

@property (weak, nonatomic) IBOutlet UILabel *timeLeftLabel;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    spinCount = MAX_NO_ATTEMPTS;
    numOfAttempt = 0;
    self.noOfAttemptsLabel.text = [NSString stringWithFormat:@"%i", spinCount];
    
    score = 0;
    isAttempted = NO;
    haveShownScoreAlert = NO;
    
//    for (NSString *family in [UIFont familyNames])
//    {
//        for (NSString *names in [UIFont fontNamesForFamilyName:family])
//        {
//            NSLog(@"%@ %@", family, names);
//        }
//    }

    repeatSlotCombinationArr = [[NSMutableArray alloc] init];
    
    slot1Items = [[DBManager sharedInstance] getSlotItemsForSlotNo:1];
    slot2Items = [[DBManager sharedInstance] getSlotItemsForSlotNo:2];
    slot3Items = [[DBManager sharedInstance] getSlotItemsForSlotNo:3];

    if ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone)
    {
        self.scoreLabel.font = [UIFont fontWithName:@"Digital-7" size:20];
        self.timeLeftLabel.font = [UIFont fontWithName:@"Digital-7" size:20];
        self.whereLbl.font = [UIFont fontWithName:@"Museo-700" size:14];
        self.whatLbl.font = [UIFont fontWithName:@"Museo-700" size:14];
        self.howLbl.font = [UIFont fontWithName:@"Museo-700" size:14];
        self.noOfAttemptsLabel.font = [UIFont fontWithName:@"Museo-700" size:14];
    }
    else
    {
        self.scoreLabel.font = [UIFont fontWithName:@"Digital-7" size:45];
        self.timeLeftLabel.font = [UIFont fontWithName:@"Digital-7" size:45];
        self.whereLbl.font = [UIFont fontWithName:@"Museo-700" size:25];
        self.whatLbl.font = [UIFont fontWithName:@"Museo-700" size:25];
        self.howLbl.font = [UIFont fontWithName:@"Museo-700" size:25];
        self.noOfAttemptsLabel.font = [UIFont fontWithName:@"Museo-700" size:20];
    }
    
    self.scoreLabel.text = [NSString stringWithFormat:@"0/%i", MAX_NO_ATTEMPTS];

    [self.spinOutlet setBackgroundImage:[UIImage imageNamed:@"spin-button-on.png"] forState:UIControlStateNormal];
    [self.spinOutlet setBackgroundImage:[UIImage imageNamed:@"spin-button-off.png"] forState:UIControlStateDisabled];
    
    self.slotOutlet1.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.slotOutlet1.minNoOfRotations = 4;
    self.slotOutlet1.delegate = self;
    self.slotOutlet1.dataSource = self;
    
    self.slotOutlet2.contentInset = UIEdgeInsetsMake(0, -4, 0, 0);
    self.slotOutlet2.minNoOfRotations = 5;
    self.slotOutlet2.delegate = self;
    self.slotOutlet2.dataSource = self;
    
    self.slotOutlet3.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.slotOutlet3.minNoOfRotations = 6;
    self.slotOutlet3.delegate = self;
    self.slotOutlet3.dataSource = self;
    
    if ([UIScreen mainScreen].bounds.size.height == 480)
    {
        self.UCBLogoImg.image = [UIImage imageNamed:@"ucb-lync-champion-logo(320*480)"];
        self.howCenterAlineConstraint.constant = 0;
    }
    
    // In ios 8 while in landscape width and height are interchanged

    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        if ([UIScreen mainScreen].bounds.size.width == 568) {
            
            NSLog(@"Height = %f", [UIScreen mainScreen].bounds.size.height);
            self.backgroundImgViweOutlet.image = [UIImage imageNamed:@"Background-1136X640.png"];
            
            self.slotOutlet1.contentInset = UIEdgeInsetsMake(0, 7, 0, 0);
            self.slotOutlet2.contentInset = UIEdgeInsetsMake(0, -3, 0, 0);
            self.slotOutlet3.contentInset = UIEdgeInsetsMake(0, -5, 0, 0);
            
            self.refreshLeftConst.constant = 45;
            self.helpRightConst.constant = 50;
            self.exceToRiskConst.constant = 50;
            self.riskT0JustDntConst.constant = 50;
            self.execlLeftConst.constant = 80;
        }
        if ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPad)
        {
            self.slotOutlet1.contentInset = UIEdgeInsetsMake(0, 20, 0, 0);
            self.slotOutlet3.contentInset = UIEdgeInsetsMake(0, -20, 0, 0);
        }
    }
    else
    {
        if ([UIScreen mainScreen].bounds.size.height == 568)
        {
            NSLog(@"Height = %f", [UIScreen mainScreen].bounds.size.height);
            self.backgroundImgViweOutlet.image = [UIImage imageNamed:@"Background-1136X640.png"];
            
            self.slotOutlet1.contentInset = UIEdgeInsetsMake(0, 7, 0, 0);
            self.slotOutlet2.contentInset = UIEdgeInsetsMake(0, -3,0, 3);
            self.slotOutlet3.contentInset = UIEdgeInsetsMake(0, -5, 0, 0);
            
            self.refreshLeftConst.constant = 45;
            self.helpRightConst.constant = 50;
            self.exceToRiskConst.constant = 50;
            self.riskT0JustDntConst.constant = 50;
            self.execlLeftConst.constant = 80;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _timeLeft = MAX_TIME_ALLOTTED;
    self.timeLeftLabel.text = [self timeFormatted:_timeLeft];

    [self resetGame];
}

- (IBAction)spinAction:(id)sender
{
    isAttempted = YES;
    
    if (spinCount == MAX_NO_ATTEMPTS)
    {
        [timer invalidate];
        timer = nil;
        [self startTimer];
    }
    
    if (spinCount <= MAX_NO_ATTEMPTS)
    {
        spinCount--;
        NSLog(@"Spin %i",spinCount);
        self.noOfAttemptsLabel.text = [NSString stringWithFormat:@"%i", spinCount];
    }
    else
    {
//        [self refreshBtnAction:self.refreshBtnOutlet];
    }

    [self disableButtons];
    currentlySelectedAnswerCode = nil;
    
//    NSUInteger slotIconCount = [slot1Icons count];
    self.slotOutlet1.slotResults = [self arrayOfRandomResultsForCount:[slot1Items count]];
    self.slotOutlet2.slotResults = [self arrayOfRandomResultsForCount:[slot2Items count]];
    self.slotOutlet3.slotResults = [self arrayOfRandomResultsForCount:[slot3Items count]];

    NSInteger randomResultForSlot1 = [self.slotOutlet1.slotResults[0] integerValue];
    NSInteger randomResultForSlot2 = [self.slotOutlet2.slotResults[0] integerValue];
    NSInteger randomResultForSlot3 = [self.slotOutlet3.slotResults[0] integerValue];
    
    NSString *codeForSlot1 = [slot1Items[randomResultForSlot1] slotCode];
    NSString *codeForSlot2 = [slot2Items[randomResultForSlot2] slotCode];
    NSString *codeForSlot3 = [slot3Items[randomResultForSlot3] slotCode];
    
    NSString *slotCombination = [NSString stringWithFormat:@"%@-%@-%@",codeForSlot1,codeForSlot2,codeForSlot3];
    currentTruthTableEntry = [[DBManager sharedInstance] getTruthTableEntryFor:slotCombination];

    NSLog(@"Validity = %li", (long)currentTruthTableEntry.truthTableEntryValid);
    
//    while ([repeatSlotCombinationArr containsObject:slotCombination] || currentTruthTableEntry.truthTableEntryValid == 0)
    while ([repeatSlotCombinationArr containsObject:slotCombination] || currentTruthTableEntry.truthTableSlotCombinationCode == nil)
    {
        NSLog(@"This combination is already existed or it is invalid");
        self.slotOutlet1.slotResults = [self arrayOfRandomResultsForCount:[slot1Items count]];
        self.slotOutlet2.slotResults = [self arrayOfRandomResultsForCount:[slot2Items count]];
        self.slotOutlet3.slotResults = [self arrayOfRandomResultsForCount:[slot3Items count]];
        
        randomResultForSlot1 = [self.slotOutlet1.slotResults[0] integerValue];
        randomResultForSlot2 = [self.slotOutlet2.slotResults[0] integerValue];
        randomResultForSlot3 = [self.slotOutlet3.slotResults[0] integerValue];
        
        codeForSlot1 = [slot1Items[randomResultForSlot1] slotCode];
        codeForSlot2 = [slot2Items[randomResultForSlot2] slotCode];
        codeForSlot3 = [slot3Items[randomResultForSlot3] slotCode];
        
        slotCombination = [NSString stringWithFormat:@"%@-%@-%@",codeForSlot1,codeForSlot2,codeForSlot3];
        currentTruthTableEntry = [[DBManager sharedInstance] getTruthTableEntryFor:slotCombination];
        NSLog(@"Validity = %li", (long)currentTruthTableEntry.truthTableEntryValid);
    }
    
    [repeatSlotCombinationArr addObject:slotCombination];
    
    NSLog(@"Created Slot Combition Code is ----   %@ and correct answer code = %@",slotCombination, currentTruthTableEntry.truthTableCorrectAnswerCodes);
    
    [self.slotOutlet1 startSliding];
    [self.slotOutlet2 startSliding];
    [self.slotOutlet3 startSliding];
}

- (IBAction)answerButtons:(id)sender
{
    if (isAttempted)
    {
        isAttempted = NO;
        numOfAttempt ++;

        self.excelentBtnOutlet.enabled = NO;
        self.justDontBtnOutlet.enabled = NO;
        self.riskyBtnOutlet.enabled = NO;
        
        self.spinOutlet.enabled = YES;
        
        if ([self checkForCorrectnessForBtnTag:[sender tag]])
        {
            score++;
            self.scoreLabel.text = [NSString stringWithFormat:@"%li/%i", (long)score, MAX_NO_ATTEMPTS];

            [self showAlertForCorrectResultForView:sender];
        }else
        {
            [self showAlertForWrongResultForView:sender];
        }
        
        if (numOfAttempt < MAX_NO_ATTEMPTS)
        {
            NSLog(@"Attempt %i",numOfAttempt);
        }
        else
        {
            
        }
    }
}

- (IBAction)showHelpButtonPressed:(UIButton *)sender
{
    [self ifNeededAllocCustomAlertView];

//if no hint is given we will set subtitle as nil
    customAlertView.subTitle = currentTruthTableEntry.truthTableHint.length == 0?nil:currentTruthTableEntry.truthTableHint;
    customAlertView.alertType = CustomAlertHelp;
    [customAlertView showForFrame:sender.frame];
}

- (BOOL)checkForCorrectnessForBtnTag:(NSInteger)tag
{
    NSString *answerCode = nil;
    
    switch (tag)
    {
        case 100:
            answerCode = @"NO_ISSUE";
            break;
            
        case 101:
            answerCode = @"RISKY";
            break;
            
        case 102:
            answerCode = @"JUST_DONT";
            break;
            
        default:
            break;
    }
    
    currentlySelectedAnswerCode = answerCode;
    
    if ([currentTruthTableEntry.truthTableCorrectAnswerCodes containsObject:answerCode])
    {
        return YES;
    }
    
    return NO;
}

- (void)showAlertForCorrectResultForView:(UIView *)sender
{
    [self ifNeededAllocCustomAlertView];

    customAlertView.subTitle = [currentTruthTableEntry explanationForAnswerCode:currentlySelectedAnswerCode];
    customAlertView.alertType = CustomAlertCorrect;
    [customAlertView showForFrame:sender.frame];
}

- (void)showAlertForWrongResultForView:(UIView *)sender
{
    [self ifNeededAllocCustomAlertView];
    
    customAlertView.subTitle = [currentTruthTableEntry explanationForAnswerCode:currentlySelectedAnswerCode];
    customAlertView.alertType = CustomAlertWrong;
    [customAlertView showForFrame:sender.frame];
}

- (void)showAlertWithScore
{
    haveShownScoreAlert = YES;
    [self ifNeededAllocCustomAlertView];
    
    customAlertView.subTitle = @"Thanks for playing the Game. Would you like to play again?";
    customAlertView.alertType = CustomAlertScore;
    [customAlertView showForFrame:self.view.frame];
}

- (void)showAlertForAlmostThereMessage
{
    haveShownScoreAlert = YES;
    [self ifNeededAllocCustomAlertView];
    
    customAlertView.subTitle = @"YOU ARE ALMOST THERE. Would you like to play again?";
    customAlertView.alertType = CustomAlertScore;
    [customAlertView showForFrame:self.view.frame];
}

- (void)disableButtons
{
    self.spinOutlet.enabled = NO;
    self.excelentBtnOutlet.enabled = NO;
    self.justDontBtnOutlet.enabled = NO;
    self.riskyBtnOutlet.enabled = NO;
}

- (void)enableButtons
{
    self.spinOutlet.enabled = YES;
    self.excelentBtnOutlet.enabled = YES;
    self.justDontBtnOutlet.enabled = YES;
    self.riskyBtnOutlet.enabled = YES;
}

- (IBAction)refreshBtnAction:(UIButton *)sender
{
    [self ifNeededAllocCustomAlertView];
    
    customAlertView.title = @"PLAY NEW GAME ?";
    
    customAlertView.alertType = CustomAlertReset;
    [customAlertView showForFrame:sender.frame];
}

- (void)startTimer
{
    if (timer == nil)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                 target:self
                                               selector:@selector(timerFired:)
                                               userInfo:nil
                                                repeats:YES];
    }
    
    _timeLeft = MAX_TIME_ALLOTTED;
    self.timeLeftLabel.text = [self timeFormatted:_timeLeft];
    [timer fire];
}

- (void)stopTimer:(NSTimer *)timerToStop
{
    [timerToStop invalidate];
    timer = nil;
    [self finishGame];
}

- (void)timerFired:(NSTimer *)firedTimer
{
    _timeLeft--;
    self.timeLeftLabel.text = [self timeFormatted:_timeLeft];

    NSLog(@"Timer left = %li", (long)_timeLeft);
    
    if (_timeLeft == 0)
    {
        [self stopTimer:firedTimer];
    }
}

- (void)finishGame
{
    [self disableButtons];
    [timer invalidate];
    timer = nil;

    [self ifNeededAllocCustomAlertView];
    
    if (score == MAX_NO_ATTEMPTS)
    {
        customAlertView.alertType = CustomAlertChampion;
        [customAlertView showForFrame:self.view.frame];
        
    }else if (!haveShownScoreAlert)
    {
        if (score >= (MAX_NO_ATTEMPTS - 3))
        {
            [self showAlertForAlmostThereMessage];
        }else
        {
            [self showAlertWithScore];
        }
    }
}

- (void)resetGame
{
    numOfAttempt = 0;
    score = 0;
    spinCount = MAX_NO_ATTEMPTS;
    self.noOfAttemptsLabel.text = [NSString stringWithFormat:@"%i", spinCount];
    haveShownScoreAlert = NO;
    
    _timeLeft = MAX_TIME_ALLOTTED;

    NSLog(@"Spin %i",spinCount);
    NSLog(@"Attempt %i",numOfAttempt);
    
    self.spinOutlet.enabled = YES;
    self.justDontBtnOutlet.enabled = NO;
    self.riskyBtnOutlet.enabled = NO;
    self.excelentBtnOutlet.enabled = NO;
    
    self.scoreLabel.text = [NSString stringWithFormat:@"0/%i", MAX_NO_ATTEMPTS];
    
    currentTruthTableEntry = nil;
    currentlySelectedAnswerCode = nil;
    [repeatSlotCombinationArr removeAllObjects];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:UPDATE_REQUIRE_KEY])
    {
        if (!updationAlert)
        {
            updationAlert = [[UIAlertView alloc] initWithTitle:@"Update"
                                                       message:@"New updates are available. Will update"
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
            updationAlert.delegate =  self;
        }
        
        [updationAlert show];
        [self stopTimer:timer];
    }else
    {
        [SyncHelper sharedInstance].delegate = self;
        [[SyncHelper sharedInstance] checkForUpdate];
//        
        [timer invalidate];
        timer = nil;
        self.timeLeftLabel.text = [self timeFormatted:_timeLeft];

//        [self startTimer];
    }
}

- (NSArray *)arrayOfRandomResultsForCount:(NSInteger)iconCount
{
    NSUInteger slotOneIndex = abs(arc4random() % iconCount);
    
    return [NSArray arrayWithObjects:[NSNumber numberWithInteger:slotOneIndex], nil];
}

- (NSArray *)slotImagesFromSlotItems:(NSArray *)slotItems
{
    NSMutableArray *slotImages = [[NSMutableArray alloc] init];
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    
    for (SlotItem *aSlotItem in slotItems)
    {
        NSMutableString *fileName = [[NSString stringWithFormat:@"%@", aSlotItem.slotFileName] mutableCopy];
        NSRange rangeOfFileName;
        rangeOfFileName.length = fileName.length;
        rangeOfFileName.location = 0;
        [fileName replaceOccurrencesOfString:@".png" withString:@"@2x.png" options:NSCaseInsensitiveSearch range:(rangeOfFileName)];
        
        NSData *imageData = [NSData dataWithContentsOfFile:[docsDir stringByAppendingPathComponent:fileName]];
        
        if (imageData)
        {
            UIImage *tempImage = [UIImage imageWithData:imageData];
            UIImage *slotimage = [UIImage imageWithCGImage:tempImage.CGImage scale:2 orientation:tempImage.imageOrientation] ;
            
            [slotImages addObject:slotimage];
        }else
        {
            
            [slotImages addObject:[UIImage imageNamed:[NSString stringWithFormat:@"%@", aSlotItem.slotFileName]]];
        }
    }

    NSLog(@"No of image is %lu", (unsigned long)[slotImages count]);
    return slotImages;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark ZCSlotMachineDelegate methods

- (NSArray *)iconsForSlotsInSlotMachine:(ZCSlotMachine *)slotMachine
{
    if ([slotMachine isEqual:self.slotOutlet1])
    {
        return [self slotImagesFromSlotItems:slot1Items];
    }else if ([slotMachine isEqual:self.slotOutlet2])
    {
        return [self slotImagesFromSlotItems:slot2Items];
    }
    return [self slotImagesFromSlotItems:slot3Items];
}

- (NSUInteger)numberOfSlotsInSlotMachine:(ZCSlotMachine *)slotMachine
{
    return 1;
}

- (CGFloat)slotWidthInSlotMachine:(ZCSlotMachine *)slotMachine
{
    if ([slotMachine isEqual:self.slotOutlet1])
    {
        return self.slotOutlet1.frame.size.width;
    }else if ( [slotMachine isEqual:self.slotOutlet2])
    {
        return self.slotOutlet2.frame.size.width;
    }
    
    return self.slotOutlet3.frame.size.width;
}

- (CGFloat)slotSpacingInSlotMachine:(ZCSlotMachine *)slotMachine
{
    return 0;
}

- (void)slotMachineWillStartSliding:(ZCSlotMachine *)slotMachine
{
    if ([slotMachine isEqual:self.slotOutlet1])
    {
        return;
    }else if ( [slotMachine isEqual:self.slotOutlet2])
    {
        return ;
    }
}

- (void)slotMachineDidEndSliding:(ZCSlotMachine *)slotMachine
{
    if ([slotMachine isEqual:self.slotOutlet1])
    {
        return;
    }else if ( [slotMachine isEqual:self.slotOutlet2])
    {
        return ;
    }
    
    if (isAttempted) {
        self.excelentBtnOutlet.enabled = YES;
        self.riskyBtnOutlet.enabled = YES;
        self.justDontBtnOutlet.enabled = YES;
    }
    
    [self enableButtons];
    self.spinOutlet.enabled = NO;
}

- (CGFloat)slotDurationFactorinSlotMachine:(ZCSlotMachine *)slotMachine
{
    if ([slotMachine isEqual:self.slotOutlet1])
    {
        return 15;
    }else if ( [slotMachine isEqual:self.slotOutlet2])
    {
        return 21;
    }
    return 27;
}

#pragma mark
#pragma mark CustomAlertViewDelegate methods
- (void)customAlertInitiatedEmail:(CustomAlertView *)customAlert
{
    MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc]init];
    mailComposer.mailComposeDelegate = self;
    mailComposer.toRecipients = @[@"imLyncchampion@ucb.com"];
//    [mailComposer setToRecipients:@[@"sundaravadivel@vmokshagroup.com"]];
    [mailComposer setSubject:@"LYNC CHAMPION"];
    [mailComposer setMessageBody:@"I am a LYNC CHAMPION !!!" isHTML:NO];
    NSLog(@"%@",mailComposer);
    
    if (mailComposer)
    {
        [self presentViewController:mailComposer animated:YES completion:nil];
    }
}

- (void)customAlertIsClosed:(CustomAlertView *)customAlert
{
    if (customAlert.alertType == CustomAlertChampion )
    {
        [self resetGame];
    }
    
    if (customAlert.alertType == CustomAlertScore)
    {
        return;
    }
    if (numOfAttempt >= MAX_NO_ATTEMPTS)
    {
        [self finishGame];
    }
}

- (void)customAlertYesButtonPressed:(CustomAlertView *)customAlert
{
    if (customAlert.alertType == CustomAlertReset || customAlert.alertType == CustomAlertScore)
    {
        [self resetGame];
        [customAlert hideAlert];
    }
}

- (void)customAlertNoButtonPressed:(CustomAlertView *)customAlert
{
    if (customAlert.alertType == CustomAlertReset)
    {
        [customAlert hideAlert];
    }else if (customAlert.alertType == CustomAlertScore)
    {
        [customAlert hideAlert];
    }
    
    [timer invalidate];
    timer = nil;
}

- (void)ifNeededAllocCustomAlertView
{
    if (!customAlertView)
    {
        customAlertView = [[CustomAlertView alloc] init];
        customAlertView.parentView = self.view;
        customAlertView.delegate = self;
    }
}

#pragma mark
#pragma mark MFMailComposeViewControllerDelegate methods
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    UIAlertView *alert;
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
            
        case MFMailComposeResultFailed:
            alert = [[UIAlertView alloc] initWithTitle:@"MyApp"
                                               message:@"Unknown Error"
                                              delegate:self
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles: nil];
            [alert show];
            break;
            
        case MFMailComposeResultSent:
            
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark
#pragma mark SyncHelperDelegate methods

- (void)syncHelper:(SyncHelper *)helper reqiuresToUpdateTable:(BOOL)updateReqiured
{
    if (updateReqiured)
    {
        
    }else
    {
        NSLog(@"No updation is required");
    }
}

- (void)syncHelperFailedToCheckUpdate:(SyncHelper *)syncHelper
{
    
}

- (void)synchelper:(SyncHelper *)helper finishedSuccessfully:(BOOL)success
{
    slot1Items = [[DBManager sharedInstance] getSlotItemsForSlotNo:1];
    slot2Items = [[DBManager sharedInstance] getSlotItemsForSlotNo:2];
    slot3Items = [[DBManager sharedInstance] getSlotItemsForSlotNo:3];
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

#pragma mark
#pragma mark UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView isEqual:updationAlert])
    {
        if (![AFNetworkReachabilityManager sharedManager].reachable)
        {
            UIAlertView *networkAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The Internet connection appears to be offline. Please check Internet connection" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [networkAlert show];
        }
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [SyncHelper sharedInstance].delegate = self;
        [[SyncHelper sharedInstance] startUpdatingRequiredTables];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    //return supported orientation masks
    return UIInterfaceOrientationMaskLandscape;
}

- (NSString *)timeFormatted:(NSInteger)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    return [NSString stringWithFormat:@"%d:%02d",minutes, seconds];
}

@end
