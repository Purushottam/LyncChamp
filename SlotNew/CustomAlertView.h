//
//  CustomAlertView.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/11/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomAlertView;

typedef NS_ENUM(NSInteger, CustomAlertType){
    CustomAlertCorrect,
    CustomAlertWrong,
    CustomAlertChampion,
    CustomAlertReset,
    CustomAlertHelp,
    CustomAlertScore
};

@protocol CustomAlertViewDelegate <NSObject>

- (void)customAlertInitiatedEmail:(CustomAlertView *)customAlert;
- (void)customAlertIsClosed:(CustomAlertView *)customAlert;

- (void)customAlertYesButtonPressed:(CustomAlertView *)customAlert;
- (void)customAlertNoButtonPressed:(CustomAlertView *)customAlert;

@end

@interface CustomAlertView : UIView

@property (strong, nonatomic) UIColor *colorOfView;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *subTitle;
@property (strong, nonatomic) NSString *explanation;

@property (assign, nonatomic) CustomAlertType alertType;

@property (weak, nonatomic) UIView *parentView;

@property (weak, nonatomic) id<CustomAlertViewDelegate> delegate;

- (void)showForFrame:(CGRect)fromFrame;
- (void)hideAlert;

@end
