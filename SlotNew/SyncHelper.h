//
//  SyncHelper.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 12/2/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>
@class SyncHelper;

@protocol SyncHelperDelegate <NSObject>

- (void)syncHelper:(SyncHelper *)helper reqiuresToUpdateTable:(BOOL)updateReqiured;
- (void)synchelper:(SyncHelper *)helper finishedSuccessfully:(BOOL)success;
- (void)syncHelperFailedToCheckUpdate:(SyncHelper *)syncHelper;

@end

@interface SyncHelper : NSObject

@property (weak, nonatomic) id <SyncHelperDelegate> delegate;

+ (SyncHelper *)sharedInstance;
- (void)checkForUpdate;

- (void)startUpdatingRequiredTables;

@end
