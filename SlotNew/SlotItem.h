//
//  SlotItem.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/14/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SlotItem : NSObject

@property (nonatomic, strong)NSString *slotCategoryCode;
@property (nonatomic ,strong)NSString *slotCategoryName;
@property (nonatomic ,strong)NSString *slotCode;
@property (nonatomic ,strong)NSString *slotDocumentCode;
@property (nonatomic ,strong)NSString *slotDocumentType;
@property (nonatomic ,strong)NSString *slotFileName;
@property (nonatomic ,strong)NSString *slotIcon;
@property (nonatomic, assign)NSInteger slotID;
@property (nonatomic ,strong)NSString *slotname;
@property (nonatomic, assign)NSInteger slotNumber;
@property (nonatomic, assign)NSInteger slotStatus;
@property (nonatomic, assign)NSInteger slotUpdateCount;

@end
