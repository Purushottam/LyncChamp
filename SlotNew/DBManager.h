//
//  DBManager.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/14/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TruthTableEntry.h"
#import "SeedTableEntry.h"

@interface DBManager : NSObject

+ (DBManager *)sharedInstance;

- (NSString *)dbPath;
- (NSArray *)getAnswerDetails;
- (NSArray *)getCategories;
- (NSArray *)getSlotItemsForSlotNo:(NSInteger) slotNo;
- (TruthTableEntry *)getTruthTableEntryFor:(NSString *)combination;
- (SeedTableEntry *)getSeedDataFromLocalForName:(NSString *)name;

@end
