//
//  DBCreater.m
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/28/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "DBCreater.h"
#import <sqlite3.h>

@implementation DBCreater
{
    NSString *databasePath;
    sqlite3 *database;
//    sqlite3_stmt *statement ;

    AFHTTPRequestOperationManager *manager;
    AFJSONRequestSerializer *requestSerializer;
    
    NSMutableArray *answerDataArr, *categoryDataArr, *slotItemsDataArr, *truthTableDataArr;
    NSMutableArray *seedArray;
}

- (id)init
{
    if (self = [super init])
    {
        manager = [AFHTTPRequestOperationManager manager];
        requestSerializer = [AFJSONRequestSerializer serializer];
        [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.requestSerializer = requestSerializer;
        
        NSString *docsDir;
        NSArray *dirPaths;
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = dirPaths[0];
        databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent:@"LyncSlot.db"]];
        
        NSLog(@"Data Base Path %@",databasePath);
    }
    
    return self;
}

- (void)copyDBToDoc
{
    NSFileManager *fileManager = [NSFileManager defaultManager];

    if (![fileManager fileExistsAtPath:databasePath])
    {
        NSLog(@"FILE NOT EXISTS, SO START COPYING");
        NSString *sourcePath = [[NSBundle mainBundle]  pathForResource:@"LyncSlot" ofType:@"db"];
        
        NSError *error;
        if ([[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:databasePath error:&error])
        {
            NSLog(@"Moved db to : %@", databasePath);
            
        }else
        {
            NSLog(@"Copying db is failed with error: %@", [error userInfo]);
        }
    }else
    {
        NSLog(@"DB is already existing");
    }
}

- (void)createTableForQuery:(NSString *)query
{
    NSLog(@"Data Base Path %@",databasePath);
    NSFileManager *filemgr = [NSFileManager defaultManager];
    const char *dbpath = [databasePath UTF8String];
    if (![filemgr fileExistsAtPath: databasePath ])
    {
        if (sqlite3_open(dbpath, &database)== SQLITE_OK)
        {
            sqlite3_close(database);
        }
    }
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt = [query UTF8String];
        
        if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
        {
            NSLog(@"Failed to create table");
        }
        sqlite3_close(database);
    }
    else {
        NSLog(@"Failed to open/create database");
    }
}

- (void)dropTable:(NSString *)tableName
{
    const char *dbPath = [databasePath UTF8String];
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        NSString *deleQry = [NSString stringWithFormat:@"DROP TABLE IF EXISTS %@", tableName];
        const char *sqlStmt = [deleQry UTF8String];
        
        sqlite3_stmt *statement;
        
        if (sqlite3_prepare_v2(database, sqlStmt, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Table %@ is droped", tableName);
            }else
            {
                NSLog(@"Table %@ is not able to dropped", tableName);
            }
        }else
        {
            NSLog(@"Statement for droping is failed to be prepared");
        }
        
        sqlite3_close(database);
    }
}

- (void)saveDataToDBForQuery:(NSString *)query
{
    const char *dbPath = [databasePath UTF8String];
    
    if (sqlite3_open(dbPath, &database) == SQLITE_OK)
    {
        
        const char *insert_stmt = [query UTF8String];
        
        sqlite3_stmt *statement;

        sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"saved Sucessfully");
        }
        else
        {
            NSLog(@"Not saved ");
        }
        
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
}

- (void)updateAnswerTable
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:UPDATE_ANSWERS_KEY])
    {
        return;
    }
    [manager POST:@"http://lsmtst.azurewebsites.net/search/answer/" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSData *data = [operation responseData];
        NSDictionary *dictionaryFromJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
        
        
        if ([dictionaryFromJSON[@"aaData"][@"Success"] boolValue])
        {
            [self dropTable:@"answer"];
            
            NSString *query = @"create table if not exists answer (id integer, name text, code text, status integer)";
            [self createTableForQuery:query];
            
            NSArray *arr = dictionaryFromJSON[@"aaData"][@"GenericSearchViewModels"];
            
            answerDataArr = [[NSMutableArray alloc] init];
            
            for (NSDictionary *aDict in arr)
            {
                if (![aDict[@"Status"] boolValue])
                {
                    continue;
                }
                
                AnswerOption *answerData = [[AnswerOption alloc] init];
                answerData.name = aDict[@"Name"];
                answerData.code = aDict[@"Code"];
                answerData.id = [aDict[@"Id"] integerValue];
                answerData.status = [aDict[@"Status"] integerValue];
                [answerDataArr addObject:answerData];
                
                NSString *insertSQL = [NSString stringWithFormat:@"insert into answer (id, name, code, status) values (%i ,\"%@\", \"%@\", %i)", answerData.id , answerData.name, answerData.code, answerData.status];
                
                [self saveDataToDBForQuery:insertSQL];
            }
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:UPDATE_ANSWERS_KEY];
            [self.delegate createrSucessfullyDownloadedAnswer:self];
        }else
        {
            if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingAnswer:)])
            {
                [self.delegate createrFailedDownloadingAnswer:self];
            }
        }
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@" error %@",error);
              if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingAnswer:)])
              {
                  [self.delegate createrFailedDownloadingAnswer:self];
              }
          }];
}

- (void)updateCategoriesTable
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:UPDATE_CATEGORY_KEY])
    {
        return;
    }
    
    [manager GET:@"http://lsmtst.azurewebsites.net/admin/category" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {

         NSData *data = [operation responseData];
         NSDictionary *dictionaryFromJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
         
         if ([dictionaryFromJSON[@"aaData"][@"Success"] boolValue])
         {
             [self dropTable:@"category"];
             
             NSString *query = @"create table if not exists category (id integer, name text, code text, status integer, slotNo integer)";
             [self createTableForQuery:query];
             
             NSArray *arr = dictionaryFromJSON[@"aaData"][@"Category"];
             
             categoryDataArr = [[NSMutableArray alloc] init];
             
             for (NSDictionary *aDict in arr)
             {
                 if (![aDict[@"Status"] boolValue])
                 {
                     continue;
                 }
                 
                 SlotCategroy *categoryData = [[SlotCategroy alloc] init];
                 categoryData.name = aDict[@"Name"];
                 categoryData.code = aDict[@"Code"];
                 categoryData.id = [aDict[@"Id"] integerValue];
                 categoryData.status = [aDict[@"Status"] integerValue];
                 categoryData.slotNo = [aDict[@"SlotNumber"] integerValue];
                 [categoryDataArr addObject:categoryData];
                 
                 NSString *insertSQL = [NSString stringWithFormat:@"insert into category (id, name, code, status, slotNo) values (%i ,\"%@\", \"%@\", %i, %i)", categoryData.id , categoryData.name, categoryData.code, categoryData.status, categoryData.slotNo];
                 
                 [self saveDataToDBForQuery:insertSQL];
             }
             
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:UPDATE_CATEGORY_KEY];
             [self.delegate createrSucessfullyDownloadedCategory:self];
             
         }else
         {
             if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingCategory:)])
             {
                 [self.delegate createrFailedDownloadingCategory:self];
             }
         }
         

     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@" error %@",error);
         
         if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingCategory:)])
         {
             [self.delegate createrFailedDownloadingCategory:self];
         }
        
     }];
}

- (void)updateSlotItems
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:UPDATE_SLOT_ITEM_KEY])
    {
        return;
    }
    
    [manager POST:@"http://lsmtst.azurewebsites.net/SlotItems" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSData *data = [operation responseData];
         
         NSDictionary *dictionaryFromJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
         
         if ([dictionaryFromJSON[@"aaData"][@"Success"] boolValue])
         {
             [self dropTable:@"slotItems"];
             
             NSString *query = @"create table if not exists slotItems (id integer, categoryCode text, categoryName text, code text, documentCode text, documentTypeCode text, fileName text, icon text, name text, slotNumber integer, status integer, updateCount integer)";
             [self createTableForQuery:query];
             
             NSArray *arr = dictionaryFromJSON[@"aaData"][@"Items"];
             
             slotItemsDataArr = [[NSMutableArray alloc] init];
             
             for (NSDictionary *aDict in arr)
             {
                 if (![aDict[@"Status"] boolValue])
                 {
                     continue;
                 }
                 
                 SlotItem *slotItemData = [[SlotItem alloc] init];
                 
                 slotItemData.slotCategoryCode = aDict[@"CategoryCode"];
                 slotItemData.slotCategoryName = aDict[@"CategoryName"];
                 slotItemData.slotCode = aDict[@"Code"];
                 slotItemData.slotDocumentCode = aDict[@"DocumentCode"];
                 slotItemData.slotDocumentType = aDict[@"DocumentTypeCode"];
                 slotItemData.slotFileName = aDict[@"FileName"];
                 slotItemData.slotIcon = aDict[@"Icon"];
                 slotItemData.slotID = [aDict[@"Id"] integerValue];
                 slotItemData.slotname = aDict[@"Name"];
                 slotItemData.slotNumber = [aDict[@"SlotNumber"] integerValue];
                 slotItemData.slotStatus = [aDict[@"Status"] integerValue];
                 slotItemData.slotUpdateCount = [aDict[@"UpdateCount"] integerValue];
                 
                 [slotItemsDataArr addObject:slotItemData];
                 
                 NSString *insertSQL = [NSString stringWithFormat:@"insert into slotItems (id, categoryCode, categoryName, code, documentCode, documentTypeCode, fileName, icon, name, slotNumber, status, updateCount) values (%i ,\"%@\", \"%@\", \"%@\",  \"%@\",\"%@\",\"%@\",\"%@\", \"%@\", %i, %i, %i)", slotItemData.slotID , slotItemData.slotCategoryCode, slotItemData.slotCategoryName, slotItemData.slotCode,  slotItemData.slotDocumentCode, slotItemData.slotDocumentType, slotItemData.slotFileName, slotItemData.slotIcon, slotItemData.slotname, slotItemData.slotNumber,slotItemData.slotStatus, slotItemData.slotUpdateCount ];
                 
                 [self saveDataToDBForQuery:insertSQL];
             }
             
             [self downloadSlotImages];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@" error %@",error);
         
         if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingSlotItems:)])
         {
             [self.delegate createrFailedDownloadingSlotItems:self];
         }
         
     }];
    
}

- (void)updateTruthTable
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:UPDATE_TRUTH_TABLE_KEY])
    {
        return;
    }
    
    [manager POST:@"http://lsmtst.azurewebsites.net/Search/TruthTable" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [self dropTable:@"truthTable"];
         
         NSString *query = @"create table if not exists truthTable (id integer,  code text, correctAnswerCode text, slotCombinationCode text, status integer, updateCount integer, hint text, explanationJSON text, validCombination integer)";
         [self createTableForQuery:query];
         
         NSData *data = [operation responseData];
         
         NSDictionary *dictionaryFromJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
         
         if ([dictionaryFromJSON[@"aaData"][@"Success"] boolValue])
         {
             NSArray *arr = dictionaryFromJSON[@"aaData"][@"TruthTable"];
             
             truthTableDataArr = [[NSMutableArray alloc] init];
             
             for (NSDictionary *aDict in arr)
             {
                 if (![aDict[@"Status"] boolValue])
                 {
                     continue;
                 }
                 
                 TruthTableEntry *truthTableData = [[TruthTableEntry alloc] init];
                 
                 truthTableData.truthTableCode = aDict[@"Code"];
                 truthTableData.truthTableCorrectAnswerCodeString = aDict[@"CorrectAnswerCode"];
                 truthTableData.truthTableID = [aDict[@"Id"] integerValue];
                 truthTableData.truthTableSlotCombinationCode = aDict[@"SlotCombinationCode"];
                 truthTableData.truthTableStatus = [aDict[@"Status"] integerValue];
                 truthTableData.truthTableUpdateCount = [aDict[@"UpdateCount"] integerValue];
                 
                 if ([aDict[@"JSON"] isKindOfClass:[NSString class]])
                 {
                     NSString *insideJSON = aDict[@"JSON"];
                     
                     NSData *dataFromInsideString = [insideJSON dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
                     
                     NSDictionary *dictFormInsideJSON = [NSJSONSerialization JSONObjectWithData:dataFromInsideString options:kNilOptions error:nil][@"request"];
                     
                     truthTableData.truthTableHint = dictFormInsideJSON[@"Hint"];
                     truthTableData.truthTableEntryValid = [dictFormInsideJSON[@"Valid"] integerValue];
                     
                     NSMutableString *JSONToSave = [dictFormInsideJSON[@"ExpAndReason"] mutableCopy];
                     [JSONToSave replaceOccurrencesOfString:@"\"" withString:@"\\'" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [JSONToSave length])];
                     
                     truthTableData.truthTableExplanationJSON = JSONToSave;
                 }
                 
                 [truthTableDataArr addObject:truthTableData];
                 
                 NSString *insertSQL = [NSString stringWithFormat:@"insert into truthTable (id,  code, correctAnswerCode, slotCombinationCode ,status, updateCount, hint, explanationJSON) values (%i ,\"%@\", \"%@\", \"%@\",  %i, %i, \"%@\", \"%@\")", truthTableData.truthTableID , truthTableData.truthTableCode, truthTableData.truthTableCorrectAnswerCodeString, truthTableData.truthTableSlotCombinationCode, truthTableData.truthTableStatus, truthTableData.truthTableUpdateCount, truthTableData.truthTableHint, truthTableData.truthTableExplanationJSON];
                 
                 [self saveDataToDBForQuery:insertSQL];
             }
             
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:UPDATE_TRUTH_TABLE_KEY];
             [self.delegate createrSucessfullyDownloadedTruthTable:self];
             
             //         NSLog(@" truthTable resonnse %@",truthTableDataArr);
         }else
         {
             if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingTruthTable:)])
             {
                 [self.delegate createrFailedDownloadingTruthTable:self];
             }
         }
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@" error %@",error);
         
         if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingTruthTable:)])
         {
             [self.delegate createrFailedDownloadingTruthTable:self];
         }
     }];
    
}

- (void)getSeedDataAndShouldUpdate:(BOOL)update
{
    [manager GET:@"http://lsmtst.azurewebsites.net/Seed"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {

             NSData *data = [operation responseData];
             NSDictionary *dictionaryFromJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
             
             if ([dictionaryFromJSON[@"aaData"][@"Success"] boolValue])
             {
                 NSArray *arr = dictionaryFromJSON[@"aaData"][@"seedmaster"];
                 
                 seedArray = [[NSMutableArray alloc] init];
                 
                 for (NSDictionary *aDict in arr)
                 {
                     
                     SeedTableEntry *seedEntry = [[SeedTableEntry alloc] init];
                     seedEntry.seedName = aDict[@"Name"];
                     seedEntry.seedCode = aDict[@"Code"];
                     seedEntry.seedID = [aDict[@"Id"] integerValue];
                     seedEntry.seedStatus = [aDict[@"Status"] integerValue];
                     seedEntry.seedUpdateCount = [aDict[@"UpdateCount"] integerValue];
                     [seedArray addObject:seedEntry];
                 }
                 
                 if (update)
                 {
                     [self updateLocalSeedTable];
                 }
                 
                 [self.delegate creater:self sucessfullyDownloadedSeedData:seedArray];
             }else
             {
                 [self.delegate createrFailedToDownloadSeedData:self];
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             NSLog(@" error %@",error);
             
             [self.delegate createrFailedToDownloadSeedData:self];
             
         }];
    
    return;
}

- (void)updateLocalSeedTable
{
    if (seedArray == nil)
    {
        [self getSeedDataAndShouldUpdate:YES];
        return;
    }
    [self dropTable:@"seedTable"];

     NSString *query = @"create table if not exists seedTable (id integer, name text, code text, status integer, updateCount integer)";
    [self createTableForQuery:query];
    
    for (SeedTableEntry *seedEntry in seedArray)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into seedTable (id, name, code, status, updateCount) values (%i ,\"%@\", \"%@\", %i, %i)", seedEntry.seedID, seedEntry.seedName, seedEntry.seedCode, seedEntry.seedStatus, seedEntry.seedUpdateCount];
        
        [self saveDataToDBForQuery:insertSQL];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:UPDATE_REQUIRE_KEY];
}

- (void)downloadSlotImages
{
    NSLog(@"Downloading images have started");
    [manager GET:@"http://lsmtst.azurewebsites.net/RenderSlotItem"
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:[operation responseData]
                                                                      options:kNilOptions
                                                                        error:nil];

             NSArray *arrayofImagesObj = jsonDict[@"aaData"][@"Images"];
             
             NSString *pathToDoc = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
             
             for (NSDictionary *anImageDictObj in arrayofImagesObj)
             {
                 NSString *slotitemCode = anImageDictObj[@"SlotItemCode"];
                 
                 NSString *imageAsString = anImageDictObj[@"Base64Model"][@"Image"];
                 NSData *imageDataFromBase64 = [[NSData alloc] initWithBase64EncodedString:imageAsString options:kNilOptions];
                 
                 UIImage *image = [self imageForDevice:[UIImage imageWithData:imageDataFromBase64]];
                 
                 NSData *imageData = UIImagePNGRepresentation(image);
                 
                 NSString *pathToImage;
                 
                 for (SlotItem *aSlotItem in slotItemsDataArr)
                 {
                     if ([aSlotItem.slotCode isEqualToString:slotitemCode])
                     {
                         NSMutableString *fileName = [aSlotItem.slotFileName mutableCopy];
                         NSRange rangeOfFileName;
                         rangeOfFileName.length = fileName.length;
                         rangeOfFileName.location = 0;
                         [fileName replaceOccurrencesOfString:@".png" withString:@"" options:NSCaseInsensitiveSearch range:rangeOfFileName];
                         pathToImage = [NSString stringWithFormat:@"%@/%@@2x.png", pathToDoc, fileName];
                         NSLog(@"%@", pathToImage);
                         break;
                     }
                 }
                 
                 [imageData writeToFile:pathToImage atomically:YES];
             }
             
             if ([jsonDict[@"aaData"][@"Success"] boolValue])
             {
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:UPDATE_SLOT_ITEM_KEY];
                 [self.delegate createrSucessfullyDownloadedSlotItems:self];
                 
             }else
             {
                 if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingSlotItems:)])
                 {
                     [self.delegate createrFailedDownloadingSlotItems:self];
                 }
             }
             
             NSLog(@"Downloading images done");
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             
             NSLog(@"Downloading failed");
             
             if ([self.delegate respondsToSelector:@selector(createrFailedDownloadingSlotItems:)])
             {
                 [self.delegate createrFailedDownloadingSlotItems:self];
             }

         }];
}

- (UIImage *)imageForDevice:(UIImage *)currentImage
{
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        return currentImage;
    }
    
    CGRect cropRect = CGRectMake(0, 0, 220, 209);
    UIGraphicsBeginImageContext(cropRect.size);
    [currentImage drawInRect:cropRect];
    UIImage *newCropedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newCropedImage;
}

@end
