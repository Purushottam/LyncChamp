//
//  CustomAlertView.m
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/11/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "CustomAlertView.h"

@interface CustomAlertView () 

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *explanationButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *explanationHeightConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *explanationWidthConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *explanationVerticalCenterConst;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleVerticalCenterConst;
@property (weak, nonatomic) IBOutlet UIImageView *resultImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *resultImageVerticalConstant;
@property (weak, nonatomic) IBOutlet UIImageView *popupImgBackground;

@end

@implementation CustomAlertView
{
    UIView *backgroundView;
    UIControl *alphaView;
    
    CGFloat initialExpHeightConst, initialExpWidhtConst, initialExpVrtCntrConst, initialTitlVrtConst,initialResultImageVrtConst;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init
{
    if ([[UIDevice currentDevice ]userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        id view = [[[NSBundle mainBundle] loadNibNamed:@"CustomAlertView" owner:nil options:nil] lastObject];
        [(CustomAlertView *)view initiateView];
        [self.yesButton.titleLabel setFont:[UIFont fontWithName:@"Museo-300" size:15]];
        [self.noButton.titleLabel setFont:[UIFont fontWithName:@"Museo-300" size:15]];
        [self.explanationButton.titleLabel setFont:[UIFont fontWithName:@"Museo-300" size:15]];
        return view;
    }
    else
    {
        id view = [[[NSBundle mainBundle] loadNibNamed:@"CustomAlertView~iPad" owner:nil options:nil] lastObject];
        [(CustomAlertView *)view initiateView];
        [self.yesButton.titleLabel setFont:[UIFont fontWithName:@"Museo-300" size:20]];
        [self.noButton.titleLabel setFont:[UIFont fontWithName:@"Museo-300" size:20]];
        [self.explanationButton.titleLabel setFont:[UIFont fontWithName:@"Museo-300" size:20]];
        return view;
    }
}

- (void)initiateView
{
//    self.layer.borderColor = [UIColor blackColor].CGColor;
//    self.layer.borderWidth = 2;
//    
//    self.layer.masksToBounds = YES;
//    self.layer.cornerRadius = 10;
//    
//    self.explanationButton.layer.borderColor = [UIColor blackColor].CGColor;
//    self.explanationButton.layer.borderWidth = 1;
//    
//    self.explanationButton.layer.masksToBounds = YES;
//    self.explanationButton.layer.cornerRadius = 10;
//    
//    self.yesButton.layer.borderColor = [UIColor blackColor].CGColor;
//    self.yesButton.layer.borderWidth = 1;
//    
//    self.yesButton.layer.masksToBounds = YES;
//    self.yesButton.layer.cornerRadius = 10;
//    
//    self.noButton.layer.borderColor = [UIColor blackColor].CGColor;
//    self.noButton.layer.borderWidth = 1;
//    
//    self.noButton.layer.masksToBounds = YES;
//    self.noButton.layer.cornerRadius = 10;
    
}

- (void)showForFrame:(CGRect)fromFrame
{
    if (!backgroundView)
    {
        backgroundView = [[UIView alloc] initWithFrame:self.parentView.bounds];
        backgroundView.backgroundColor = [UIColor clearColor];
    }
    
    if (!alphaView)
    {
        alphaView = [[UIControl alloc] initWithFrame:self.parentView.bounds];
        alphaView.backgroundColor = [UIColor grayColor];
        alphaView.alpha = .4;
        [alphaView addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }

    backgroundView.alpha = 0;
    NSLog(@"center %@", NSStringFromCGPoint(self.parentView.center));
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    self.center = self.parentView.center;
    
    if ((interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight) && SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        NSLog(@"is in lanscape");
        self.center = CGPointMake(self.center.y, self.center.x);
    }
    
    CGRect frameOfSelf = self.frame;
    
    self.frame = fromFrame;
    
    [self.parentView addSubview:backgroundView];
    [backgroundView addSubview:alphaView];
    [backgroundView addSubview:self];
    
    [UIView animateWithDuration:.5
                     animations:^{
                         backgroundView.alpha = 1;
                         self.frame = frameOfSelf;
                     }];
}

- (void)setColorOfView:(UIColor *)colorOfView
{
    _colorOfView = colorOfView;
    self.backgroundColor = colorOfView;
}

- (void)setTitle:(NSString *)title
{
    _title = title;
}

- (void)setSubTitle:(NSString *)subTitle
{
    _subTitle = subTitle;
    
}

- (void)setAlertType:(CustomAlertType)alertType
{
    _alertType = alertType;
    
    initialExpHeightConst = self.explanationHeightConst.constant;
    initialExpWidhtConst = self.explanationWidthConst.constant;
    initialExpVrtCntrConst = self.explanationVerticalCenterConst.constant;
    initialTitlVrtConst = self.titleVerticalCenterConst.constant;
    initialResultImageVrtConst = self.resultImageVerticalConstant.constant;
    switch (alertType)
    {
        case CustomAlertCorrect:
            [self setUpForCorrectResult];
            break;
            
        case CustomAlertWrong:
            [self setUpForWrongResult];
            break;
            
        case CustomAlertChampion:
            [self setUpViewForChampion];
            break;
            
        case CustomAlertScore:
            [self setUpViewForScore];
            break;
            
        case CustomAlertHelp:
            [self setUpViewForHelp];
            break;
            
        case CustomAlertReset:
            [self setUpViewForReset];
            break;
            
        default:
            break;
    }
}

- (void)setUpForCorrectResult
{
//    self.backgroundColor = [UIColor colorWithRed:.08 green:.59 blue:.31 alpha:1];
//    self.titleLabel.text = @"AWESOME !!";
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];

    self.resultImage.image = [UIImage imageNamed:@"Smiley-Awesome.png"];
    self.titleLabel.font = [UIFont fontWithName:@"Museo-500" size:20];

    self.titleLabel.hidden = YES;
    self.emailButton.hidden = YES;
    self.explanationButton.hidden = NO;
    self.yesButton.hidden = YES;
    self.noButton.hidden = YES;
    self.subTitleLabel.hidden = YES;
    [self.subTitleLabel setTextColor:[UIColor whiteColor]];

}

- (void)setUpForWrongResult
{
//    self.backgroundColor = [UIColor colorWithRed:.59 green:.08 blue:.05 alpha:1];
//    self.titleLabel.text = @"NOOO...!!";
    
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];

    self.resultImage.image = [UIImage imageNamed:@"Smiley-No"];
    self.resultImage.hidden = NO;
    self.titleLabel.font = [UIFont fontWithName:@"Museo-500" size:20];
    
    self.titleLabel.hidden = YES;
    self.emailButton.hidden = YES;
    self.explanationButton.hidden = NO;
    NSLog(@"%@", NSStringFromCGRect(self.explanationButton.frame));
    self.yesButton.hidden = YES;
    self.noButton.hidden = YES;
    self.subTitleLabel.hidden = YES;
    [self.subTitleLabel setTextColor:[UIColor whiteColor]];
}

- (void)setUpViewForChampion
{
//    self.backgroundColor = [UIColor colorWithRed:.08 green:.59 blue:.31 alpha:1];
//    self.titleLabel.text = @"YOU ARE A LYNC CHAMPION !!!";
    
    self.popupImgBackground.image = [UIImage imageNamed:@"you_Are_Lync_Champ.png"];


    self.titleLabel.font = [UIFont fontWithName:@"Museo-700" size:20];
    self.titleLabel.text = @"";
    self.resultImage.hidden = YES;
    self.emailButton.hidden = NO;
    self.explanationButton.hidden = YES;
    self.yesButton.hidden = YES;
    self.noButton.hidden = YES;
    self.subTitleLabel.hidden = YES;
}

- (void)setUpViewForScore
{
//    self.backgroundColor = [UIColor colorWithRed:1 green:.99 blue:.81 alpha:1];
    self.titleLabel.text = @"";
    self.subTitleLabel.text = self.subTitle;
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
//        self.subTitleLabel.font = [UIFont systemFontOfSize:15];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:15];

    }
    else
    {
//        self.subTitleLabel.font = [UIFont systemFontOfSize:25];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:20];

    }
    self.subTitleLabel.textAlignment = NSTextAlignmentCenter;

    self.resultImage.hidden = YES;
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.subTitleLabel setTextColor:[UIColor whiteColor]];

    self.emailButton.hidden = YES;
    self.explanationButton.hidden = YES;
    self.yesButton.hidden = NO;
    self.noButton.hidden = NO;
    self.subTitleLabel.hidden = NO;
}

- (void)setUpViewForHelp
{
//    self.backgroundColor = [UIColor colorWithRed:1 green:.99 blue:.81 alpha:1];
    self.titleLabel.text = @"";
    self.subTitleLabel.text = self.subTitle?: @"Help for current combination will be given here";
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];

    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
//        self.subTitleLabel.font = [UIFont systemFontOfSize:15];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:15];
    }
    else
    {
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:20];

    }
    self.subTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.resultImage.hidden  = YES;
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    [self.subTitleLabel setTextColor:[UIColor whiteColor]];

    self.emailButton.hidden = YES;
    self.explanationButton.hidden = YES;
    self.yesButton.hidden = YES;
    self.noButton.hidden = YES;
    self.subTitleLabel.hidden = NO;
}

- (void)setUpViewForReset
{
//    self.backgroundColor = [UIColor colorWithRed:1 green:.99 blue:.81 alpha:1];
    self.titleLabel.text = self.title;
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];

    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        //                             self.subTitleLabel.font = [UIFont systemFontOfSize:15];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-700" size:15];
        self.titleLabel.font = [UIFont fontWithName:@"Museo-700" size:18];

        
    }
    else
    {
        //                             self.subTitleLabel.font = [UIFont systemFontOfSize:25];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-700" size:20];
        self.titleLabel.font = [UIFont fontWithName:@"Museo-700" size:25];

    }

    self.emailButton.hidden = YES;
    self.explanationButton.hidden = YES;
    self.yesButton.hidden = NO;
    self.noButton.hidden = NO;
    self.subTitleLabel.hidden = YES;
    self.resultImage.hidden = YES;
}

- (void)resetView
{
    self.explanationHeightConst.constant = initialExpHeightConst;
    self.explanationWidthConst.constant = initialExpWidhtConst;
    self.explanationVerticalCenterConst.constant = initialExpVrtCntrConst;
    self.titleVerticalCenterConst.constant = initialTitlVrtConst;
    self.resultImageVerticalConstant.constant = initialResultImageVrtConst;
    self.titleLabel.alpha = 1;
    self.resultImage.alpha = 1;
    self.resultImage.hidden = NO;
    self.titleLabel.hidden = NO;
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];


    [self.titleLabel setTextColor:[UIColor whiteColor]];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        //                             self.subTitleLabel.font = [UIFont systemFontOfSize:15];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-700" size:14];
        
    }
    else
    {
        //                             self.subTitleLabel.font = [UIFont systemFontOfSize:25];
        self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:20];
        
    }

    self.explanationButton.enabled = YES;
    self.yesButton.hidden = YES;
    self.noButton.hidden = YES;
    self.subTitleLabel.hidden = YES;
    self.explanationButton.alpha = 1;
    
    [self.subTitleLabel setTextColor:[UIColor whiteColor]];
}

- (IBAction)closeButtonPressed:(UIButton *)sender
{
    [self hideAlert];
}

- (void)hideAlert
{
    [UIView animateWithDuration:.4
                     animations:^{
                         
                         backgroundView.alpha = 0;
                         
                     } completion:^(BOOL finished) {
                         
                         [alphaView removeFromSuperview];
                         [self removeFromSuperview];
                         [backgroundView removeFromSuperview];
                         [self resetView];
                         
                         if ([self.delegate respondsToSelector:@selector(customAlertIsClosed:)])
                         {
                             [self.delegate customAlertIsClosed:self];
                         }
                     }];
}

- (IBAction)explanationIsRequested:(UIButton *)sender
{
    sender.enabled = NO;
    
    initialExpHeightConst = self.explanationHeightConst.constant;
    initialExpWidhtConst = self.explanationWidthConst.constant;
    initialExpVrtCntrConst = self.explanationVerticalCenterConst.constant;
    initialTitlVrtConst = self.titleVerticalCenterConst.constant;
    initialResultImageVrtConst = self.resultImageVerticalConstant.constant;
    self.popupImgBackground.image = [UIImage imageNamed:@"popUpBackground.png"];

    
    [UIView animateWithDuration:.5
                     animations:^{
    
                         self.explanationHeightConst.constant = 80;
                         self.explanationWidthConst.constant = 250;
                         self.explanationVerticalCenterConst.constant = 0;
                         self.titleVerticalCenterConst.constant = 120;
                         self.titleLabel.alpha = 0;
                         self.resultImageVerticalConstant.constant = 120;
                         self.resultImage.alpha = 0;
                         self.explanationButton.alpha = 0;
                         [self layoutIfNeeded];
                         
                     } completion:^(BOOL finished) {
                         
                         self.subTitleLabel.hidden = NO;
                         self.subTitleLabel.text = self.subTitle?: @"Explanation for current combination will be shown here";
                         
                         if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
                         {
//                             self.subTitleLabel.font = [UIFont systemFontOfSize:15];
                             self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:14];
                         }
                         else
                         {
//                             self.subTitleLabel.font = [UIFont systemFontOfSize:25];
                             self.subTitleLabel.font = [UIFont fontWithName:@"Museo-500" size:20];
                         }

                         self.subTitleLabel.textAlignment = NSTextAlignmentCenter;
                         [self.subTitleLabel setTextColor:[UIColor whiteColor]];

                         self.subTitle = nil;
                     }];
}

- (IBAction)yesButtonPressed:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(customAlertYesButtonPressed:)])
    {
        [self.delegate customAlertYesButtonPressed:self];
    }
}

- (IBAction)noButtonPressed:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(customAlertNoButtonPressed:)])
    {
        [self.delegate customAlertNoButtonPressed:self];
    }
}

- (IBAction)emailResult:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(customAlertInitiatedEmail:)])
    {
        [self.delegate customAlertInitiatedEmail:self];
    }
}

@end
