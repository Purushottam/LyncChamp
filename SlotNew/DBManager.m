//
//  DBManager.m
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/14/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "DBManager.h"
#import <sqlite3.h>
#import "AnswerOption.h"
#import "SlotCategroy.h"
#import "SlotItem.h"

@implementation DBManager
{
    sqlite3 *db;
    NSString *dbPath;
}

+ (DBManager *)sharedInstance
{
    // 1
    static DBManager *_sharedInstance = nil;
    
    // 2
    static dispatch_once_t oncePredicate;
    
    // 3
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[DBManager alloc] init];
    });
    
    return _sharedInstance;
}

- (NSString *)dbPath
{
    if (!dbPath)
    {
        NSString *docsDir;
        NSArray *dirPaths;
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = dirPaths[0];
        dbPath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent:@"LyncSlot.db"]];
        NSLog(@"dbpath %@", dbPath);

    }    
    
    return dbPath;
}

- (NSArray *)getAnswerDetails
{
    const char *dbPathUTF8 = [[self dbPath] UTF8String];
    NSMutableArray *answers = [[NSMutableArray alloc] init];
    
    if (sqlite3_open(dbPathUTF8, &db) == SQLITE_OK)
    {
        NSString *queryString = @"SELECT * FROM answer";
        const char *queryUTF = [queryString UTF8String];
        
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(db, queryUTF, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                AnswerOption *anOption = [[AnswerOption alloc] init];
                
                anOption.id = sqlite3_column_int(statement, 0);
                anOption.name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                anOption.code = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                anOption.status = sqlite3_column_int(statement, 3);
                
//                NSLog(@"Answer %i, %@, %@, %i", anOption.id, anOption.name, anOption.code, anOption.status);
                [answers addObject:anOption];
            }
            sqlite3_finalize(statement);
        }
        
        sqlite3_close(db);
    }else
    {
        NSLog(@"Unable to open db");
    }
    
    return answers;
}

- (NSArray *)getCategories
{
    const char *dbPathUTF8 = [[self dbPath] UTF8String];
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    
    if (sqlite3_open(dbPathUTF8, &db) == SQLITE_OK)
    {
        NSString *queryString = @"SELECT * FROM category";
        const char *queryUTF = [queryString UTF8String];
        
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(db, queryUTF, -1, &statement, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                SlotCategroy *aCategory = [[SlotCategroy alloc] init];
                
                aCategory.id = sqlite3_column_int(statement, 0);
                aCategory.name = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                aCategory.code = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                aCategory.status = sqlite3_column_int(statement, 3);
                
//                NSLog(@"Categories %i, %@, %@, %i", aCategory.id, aCategory.name, aCategory.code, aCategory.status);
                [categories addObject:aCategory];
            }
            sqlite3_finalize(statement);
        }
        
        sqlite3_close(db);
    }else
    {
        NSLog(@"Unable to open db");
    }
    
    
    return categories;
}

- (NSArray *)getSlotItemsForSlotNo:(NSInteger)slotNo
{
    const char *dbPathUTF8 = [[self dbPath] UTF8String];
    NSMutableArray *slotItems = [[NSMutableArray alloc] init];
    
    if (sqlite3_open(dbPathUTF8, &db) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM slotItems WHERE slotNumber = '%i'", slotNo];
        const char *queryUTF = [queryString UTF8String];
        
        sqlite3_stmt *statment;
        if (sqlite3_prepare_v2(db, queryUTF, -1, &statment, NULL) == SQLITE_OK)
        {
            while (sqlite3_step(statment) == SQLITE_ROW)
            {
                SlotItem *aSlotItems = [[SlotItem alloc] init];
                aSlotItems.slotID = sqlite3_column_int(statment, 0);
                aSlotItems.slotCategoryCode = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statment, 1)];
                aSlotItems.slotCategoryName = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statment, 2)];
                aSlotItems.slotCode = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statment, 3)];
                aSlotItems.slotFileName = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statment, 6)];
                
//                NSLog(@"slotItems %i, %@, %@, %@", aSlotItems.slotID, aSlotItems.slotCategoryCode, aSlotItems.slotCategoryName, aSlotItems.slotCode);
                [slotItems addObject:aSlotItems];
            }
            sqlite3_finalize(statment);
            
        }
        sqlite3_close(db);
        
    }else
    {
        NSLog(@"Unable to open db");
    }
    
    
    return slotItems;
}

- (TruthTableEntry *)getTruthTableEntryFor:(NSString *)combination
{
    const char *dbPathUTF8 = [[self dbPath] UTF8String];
    
    TruthTableEntry *anEntry = [[TruthTableEntry alloc] init];
    
    if (sqlite3_open(dbPathUTF8, &db) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM truthTable WHERE slotCombinationCode = \"%@\"",combination];
        const char *queryUTF = [queryString UTF8String];
        
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(db, queryUTF, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                anEntry.truthTableID = sqlite3_column_int(statement, 0);
                anEntry.truthTableCode = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                
                anEntry.truthTableCorrectAnswerCodes = [[NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 2)] componentsSeparatedByString:@","];
                anEntry.truthTableSlotCombinationCode = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 3)];
                anEntry.truthTableStatus = sqlite3_column_int(statement, 4);
                anEntry.truthTableUpdateCount = sqlite3_column_int(statement, 5);
                
                anEntry.truthTableHint = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 6)];
                NSMutableString *JSONString = [[NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 7)] mutableCopy];
                [JSONString replaceOccurrencesOfString:@"\\'" withString:@"\"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [JSONString length])];
                anEntry.truthTableExplanationArray = [NSJSONSerialization JSONObjectWithData:[JSONString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:nil];
                
                anEntry.truthTableEntryValid = sqlite3_column_int(statement, 8);
                
//                NSLog(@"Truth table enrty %i ,\"%@\", \"%@\", \"%@\",  %i, %i, \"%@\", \"%@\")", anEntry.truthTableID , anEntry.truthTableCode, anEntry.truthTableCorrectAnswerCodes, anEntry.truthTableSlotCombinationCode,  anEntry.truthTableStatus, anEntry.truthTableUpdateCount, anEntry.truthTableHint, anEntry.truthTableExplanationArray);
            }
            
            sqlite3_finalize(statement);
        }
        
        sqlite3_close(db);
    }else
    {
        NSLog(@"Unable to open db");
    }
    
    return anEntry;
}

- (SeedTableEntry *)getSeedDataFromLocalForName:(NSString *)name
{
    SeedTableEntry *aSeedEntry;
    const char *dbPathUTF8 = [[self dbPath] UTF8String];
    
    if (sqlite3_open(dbPathUTF8, &db) == SQLITE_OK)
    {
        NSString *queryString = [NSString stringWithFormat:@"SELECT * FROM seedTable WHERE name = '%@'", name];
        const char *queryUTF = [queryString UTF8String];
        
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(db, queryUTF, -1, &statement, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(statement) == SQLITE_ROW)
            {
                aSeedEntry = [[SeedTableEntry alloc] init];
                
                aSeedEntry.seedID = sqlite3_column_int(statement, 0);
                aSeedEntry.seedName = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 1)];
                aSeedEntry.seedCode = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(statement, 2)];
                aSeedEntry.seedStatus = sqlite3_column_int(statement, 3);
                aSeedEntry.seedUpdateCount = sqlite3_column_int(statement, 4);

                NSLog(@"Seed entry %i, %@, %@, %i, %i", aSeedEntry.seedID, aSeedEntry.seedName, aSeedEntry.seedCode, aSeedEntry.seedStatus, aSeedEntry.seedUpdateCount);
            }
            sqlite3_finalize(statement);
        }
        
        sqlite3_close(db);
    }else
    {
        NSLog(@"Unable to open db");
    }
    
    return aSeedEntry;
}

@end
