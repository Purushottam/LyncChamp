//
//  TruthTableEntry.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/14/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TruthTableEntry : NSObject

@property (nonatomic, strong)NSString *truthTableCode;
@property (nonatomic ,strong)NSArray *truthTableCorrectAnswerCodes;
@property (nonatomic ,strong)NSString *truthTableCorrectAnswerCodeString;
@property (nonatomic, assign)NSInteger truthTableID;
@property (nonatomic ,strong)NSString *truthTableSlotCombinationCode;
@property (nonatomic, assign)NSInteger truthTableStatus;
@property (nonatomic, assign)NSInteger truthTableUpdateCount;

@property (nonatomic ,strong)NSString *truthTableHint;
@property (nonatomic ,strong)NSArray *truthTableExplanationArray;
@property (nonatomic ,strong)NSString *truthTableExplanationJSON;

@property (nonatomic, assign)NSInteger truthTableEntryValid;

- (NSString *)explanationForAnswerCode:(NSString *)answerCode;

@end
