//
//  SplashDelayViewController.m
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 12/5/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "SplashDelayViewController.h"
#import "DBCreater.h"
#import "SyncHelper.h"

@interface SplashDelayViewController () 
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImgOutlet;

@end

@implementation SplashDelayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    self.navigationController.navigationBarHidden = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        if ([UIScreen mainScreen].bounds.size.width == 568)
        {
             self.backgroundImgOutlet.image = [UIImage imageNamed:@"SplashDelay.png"];
        }else
        {
             self.backgroundImgOutlet.image = [UIImage imageNamed:@"SplashDelay960X640.png"];
        }
    }
    else
    {
        self.backgroundImgOutlet.image = [UIImage imageNamed:@"SplashDelay_ipad.png"];        
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
//    self.backgroundImgOutlet.image = [UIImage imageNamed:@"LaunchImage"];
    
    DBCreater *creater = [[DBCreater alloc] init];
    [creater copyDBToDoc];
//    
//    [SyncHelper sharedInstance].delegate = self;
//    [[SyncHelper sharedInstance] checkForUpdate];
    [self performSegueWithIdentifier:@"splashDelayToGameSegue" sender:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)synchelper:(SyncHelper *)helper finishedSuccessfully:(BOOL)success
{
    [self performSegueWithIdentifier:@"splashDelayToGameSegue" sender:nil];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations{
    //return supported orientation masks
    return UIInterfaceOrientationMaskLandscape;
}


@end
