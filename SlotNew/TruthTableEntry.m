//
//  TruthTableEntry.m
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/14/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "TruthTableEntry.h"

@implementation TruthTableEntry

- (NSString *)explanationForAnswerCode:(NSString *)answerCode
{
    
    if (!self.truthTableExplanationArray)
    {
        return nil;
    }
    
    for (NSDictionary *anExp in self.truthTableExplanationArray)
    {
        if ([answerCode isEqualToString:anExp[@"Answer"]])
        {
            return anExp[@"Explanation"];
        }
    }
    return nil;
}

@end
