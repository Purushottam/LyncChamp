//
//  SyncHelper.m
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 12/2/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import "SyncHelper.h"
#import "DBCreater.h"
#import "SeedTableEntry.h"
#import "DBManager.h"

@interface SyncHelper () <DBCreaterDelegate>

@end

@implementation SyncHelper
{
    DBCreater *creater;
    NSArray *seedArray;
    NSInteger noOfUpdatesRequirements, noOfFailedUpdates, noOfSuccessfulUpdates;
    
    BOOL checkingForUpdate, isUpadating;
}

+ (SyncHelper *)sharedInstance
{
    static SyncHelper *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[SyncHelper alloc] init];
    });
    
    return _sharedInstance;
}

- (instancetype)init
{
    if (self = [super init])
    {
        checkingForUpdate = NO;
        isUpadating = NO;
        creater = [[DBCreater alloc] init];
        creater.delegate = self;
    }
    
    return self;
}

- (void)checkForUpdate
{
//If checking is happening we dont need to restart checking
    if (checkingForUpdate)
    {
        if ([self.delegate respondsToSelector:@selector(syncHelperFailedToCheckUpdate:)])
        {
            [self.delegate syncHelperFailedToCheckUpdate:self];
        }
        
        return;
    }
    

    checkingForUpdate = YES;
    
    [creater getSeedDataAndShouldUpdate:NO];
}

- (BOOL)checkForUpdationForEntry:(NSString *)name
{
    SeedTableEntry *localSeedEntry = [[DBManager sharedInstance] getSeedDataFromLocalForName:name];
    SeedTableEntry *latestEntry;
    
    for (latestEntry in seedArray)
    {
        if ([latestEntry.seedName isEqualToString:name])
        {
            break;
        }
    }

    if (localSeedEntry.seedUpdateCount < latestEntry.seedUpdateCount)
    {
        NSLog(@"UPDATE %@", [name capitalizedString]);
        return YES;
    }
    
    NSLog(@"No need for updating %@", name);
    return NO;
}

- (BOOL)findTablesThatNeedsUpdation
{
    BOOL updationIsReqiured = NO;
    
    if ([self checkForUpdationForEntry:@"category"])
    {
        noOfUpdatesRequirements++;
        updationIsReqiured = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:UPDATE_CATEGORY_KEY];
    }
    
    if ([self checkForUpdationForEntry:@"answer"])
    {
        noOfUpdatesRequirements++;
        updationIsReqiured = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:UPDATE_ANSWERS_KEY];
    }
    
    if ([self checkForUpdationForEntry:@"slotitem"])
    {
        noOfUpdatesRequirements++;
        updationIsReqiured = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:UPDATE_SLOT_ITEM_KEY];
    }
    
    if ([self checkForUpdationForEntry:@"truthtable"])
    {
        noOfUpdatesRequirements++;
        updationIsReqiured = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:UPDATE_TRUTH_TABLE_KEY];
    }
    
    if (updationIsReqiured)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:UPDATE_REQUIRE_KEY];
        
    }else
    {
//System is upto date
//        [self.delegate synchelper:self finishedSuccessfully:YES];
    }
    
    return updationIsReqiured;
}

- (void)startUpdatingRequiredTables
{
    if (isUpadating)
    {
        [self.delegate synchelper:self finishedSuccessfully:NO];
        return;
    }
    
    [creater updateCategoriesTable];
    [creater updateAnswerTable];
    [creater updateSlotItems];
    [creater updateTruthTable];
}

#pragma mark
#pragma mark DBCreaterDelegate
- (void)creater:(DBCreater *)creater sucessfullyDownloadedSeedData:(NSArray *)seedData
{
    seedArray = seedData;
    checkingForUpdate = NO;
    
    if ([self findTablesThatNeedsUpdation])
    {
        [self.delegate syncHelper:self reqiuresToUpdateTable:YES];
    }else
    {
        [self.delegate syncHelper:self reqiuresToUpdateTable:NO];
    }
}

- (void)createrFailedToDownloadSeedData:(DBCreater *)creator
{
    checkingForUpdate = NO;
    
    if ([self.delegate respondsToSelector:@selector(syncHelperFailedToCheckUpdate:)])
    {
        [self.delegate syncHelperFailedToCheckUpdate:self];
    }
}

- (void)createrSucessfullyDownloadedSlotItems:(DBCreater *)creater
{
    [self successfullyUpdatedOneTable];
}

- (void)createrFailedDownloadingSlotItems:(DBCreater *)creater
{
    [self failedToUpdateOneTable];
}

- (void)createrSucessfullyDownloadedAnswer:(DBCreater *)creater
{
    [self successfullyUpdatedOneTable];
}

- (void)createrFailedDownloadingAnswer:(DBCreater *)creater
{
    [self failedToUpdateOneTable];
}

- (void)createrSucessfullyDownloadedCategory:(DBCreater *)creater
{
    [self successfullyUpdatedOneTable];
}

- (void)createrFailedDownloadingCategory:(DBCreater *)creater
{
    [self failedToUpdateOneTable];
}

- (void)createrSucessfullyDownloadedTruthTable:(DBCreater *)creater
{
    [self successfullyUpdatedOneTable];
}

- (void)createrFailedDownloadingTruthTable:(DBCreater *)creater
{
    [self failedToUpdateOneTable];
}

- (void)successfullyUpdatedOneTable
{
    noOfSuccessfulUpdates++;
    
    NSInteger noOfRemainingUpadtes = noOfUpdatesRequirements - (noOfFailedUpdates + noOfSuccessfulUpdates);
    
//when noOfSuccessfulUpdates == noOfUpdatesRequirements all downloadings were sucessfull
    if (noOfSuccessfulUpdates >= noOfUpdatesRequirements)
    {
        isUpadating = NO;
        [creater updateLocalSeedTable];
        [self.delegate synchelper:self finishedSuccessfully:YES];
        
    }else if (noOfRemainingUpadtes == 0) // when noOfRemainingUpadtes == 0 and some failed
    {
        isUpadating = NO;
        [self.delegate synchelper:self finishedSuccessfully:NO];
    }
}

- (void)failedToUpdateOneTable
{
    noOfFailedUpdates++;
    
    NSInteger noOfRemainingUpadtes = noOfUpdatesRequirements - (noOfFailedUpdates + noOfSuccessfulUpdates);

    if (noOfRemainingUpadtes <= 0 )
    {
        isUpadating = NO;
        [self.delegate synchelper:self finishedSuccessfully:NO];
    }
}

@end
