//
//  SlotCategroy.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 11/14/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SlotCategroy : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *code;
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) NSInteger slotNo;

@end
