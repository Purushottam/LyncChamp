//
//  SeedTableEntry.h
//  LyncSlot Machine Sumanth
//
//  Created by Varghese Simon on 12/2/14.
//  Copyright (c) 2014 Vmoksha. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SeedTableEntry : NSObject

@property (assign, nonatomic) NSInteger seedID;
@property (strong, nonatomic) NSString *seedName;
@property (strong, nonatomic) NSString *seedCode;
@property (assign, nonatomic) NSInteger seedStatus;
@property (assign, nonatomic) NSInteger seedUpdateCount;

@end
